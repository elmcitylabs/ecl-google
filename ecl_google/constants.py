
import os

try:
    from django.conf import settings
except:
    settings = {}

GOOGLE_API_BASE = "https://www.googleapis.com/%(name)s/v%(version)s/"
GOOGLE_API_VERSIONLESS_BASE = "https://www.googleapis.com/%(name)s/"

GOOGLE_OAUTH_ENDPOINT = "https://accounts.google.com/o/oauth2/auth"
GOOGLE_OAUTH_TOKEN_ENDPOINT = "https://accounts.google.com/o/oauth2/token"

GOOGLE_ANALYTICS_VERSION = "3"
GOOGLE_PREDICTION_VERSION = "1.4"
GOOGLE_TASKS_VERSION = "1"
GOOGLE_LATITUDE_VERSION = "1"
GOOGLE_BOOKS_VERSION = "1"
GOOGLE_MODERATOR_VERSION = "1"
GOOGLE_SITEVERIFICATION_VERSION = "1"
GOOGLE_URLSHORTENER_VERSION = "1"
GOOGLE_CUSTOMSEARCH_VERSION = "1"
GOOGLE_BUZZ_VERSION = "1"

if hasattr(settings, 'GOOGLE_OAUTH_KEY'):
    GOOGLE_OAUTH_KEY = settings.GOOGLE_OAUTH_KEY
else:
    GOOGLE_OAUTH_KEY = getattr(os.environ, 'GOOGLE_OAUTH_KEY', None)

if hasattr(settings, 'GOOGLE_OAUTH_SECRET'):
    GOOGLE_OAUTH_SECRET = settings.GOOGLE_OAUTH_SECRET
else:
    GOOGLE_OAUTH_SECRET = getattr(os.environ, 'GOOGLE_OAUTH_SECRET', None)

if hasattr(settings, 'GOOGLE_OAUTH_REDIRECT'):
    GOOGLE_OAUTH_REDIRECT = settings.GOOGLE_OAUTH_REDIRECT
else:
    GOOGLE_OAUTH_REDIRECT = getattr(os.environ, 'GOOGLE_OAUTH_REDIRECT', None)

if hasattr(settings, 'GOOGLE_API_KEY'):
    GOOGLE_API_KEY = settings.GOOGLE_API_KEY
else:
    GOOGLE_API_KEY = getattr(os.environ, 'GOOGLE_API_KEY', None)

