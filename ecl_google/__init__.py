# -*- coding: utf-8 -*-

"""
ECL Google
~~~~~~~~~~

:copyright: (c) 2012 Elm City Labs, LLC
:license: Apache 2.0, see LICENSE for more details.

"""

__version__ = "0.2.14"
__author__ = "Dan Loewenherz"
__copyright__ = "Copyright 2012, Elm City Labs, LLC"
__maintainer__ = "Dan Loewenherz"
__email__ = "dan@elmcitylabs.com"
__license__ = "Apache 2.0"


