#!/usr/bin/env/python

import ecl_google

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

setup(
    name = 'ecl_google',
    version = ecl_google.__version__,
    url = "http://git.elmcitylabs.com/ecl_google",
    license = ecl_google.__license__,
    description = "Google API integration library.",
    author = ecl_google.__author__,
    author_email = ecl_google.__email__,
    packages=['ecl_google'],
    install_requires=['objectifier'],
    package_data={'': ['LICENSE']},
)

