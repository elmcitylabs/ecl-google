#!/bin/bash

git add .
git ci -m "bump version to $1"
git archive --format=tar master > ecl_google-$1.tar
gzip -f ecl_google-$1.tar
s3cmd put ecl_google-$1.tar.gz s3://packages.elmcitylabs.com/ -P
